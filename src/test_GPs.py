#!/usr/bin/env python


from Fall_risk_assesment import Environment_Image, FallRiskAssesment
import numpy as np
from scipy import *
from Gaussian_processes import MGPR
import random
from shapely.geometry import Polygon, Point
import pickle
from GPs_learning import motion_set
import tensorflow as tf
from tensorflow_probability import distributions as tfd
import gpflow
from gpflow.utilities import to_default_float
from visualization import plot_intent_prediction

if __name__ == '__main__':

    design_name = "Room-2-Inboard-Footwall"
    path = "/home/roya/catkin_ws/src/risk_aware_planning"
    background_filename = "{0}/Room_Designs/{1}_objects_rotated.png".format(path, design_name)

    pickle_env = open("data/{0}/env.pickle".format(design_name),"rb")
    env = pickle.load(pickle_env)
    pickle_env.close()
    # pickle_baseline_evaluation = open("data/{0}/baseline_evaluation.pickle".format(design_name),"rb")
    # fall_risk = pickle.load(pickle_baseline_evaluation)
    # pickle_baseline_evaluation.close()
    pickle_trajectories = open("data/{0}/trajectories.pickle".format(design_name),"rb")
    patient_traj = pickle.load(pickle_trajectories)
    pickle_trajectories.close()
    pickle_motion = open("data/{0}/motion_GPs.pickle".format(design_name),"rb")
    patient_motion = pickle.load(pickle_motion)
    pickle_motion.close()

    trial_num = 1
    for trial in range(trial_num):
        initial = random.randint(0, len(patient_motion.starts)-2)

        print("**************** Generate random trajectory *****************")
        goal_rand = random.randint(0, len(patient_motion.ends[initial])-1)
        trajectory = patient_traj.traj_plan(initial,goal_rand)
        path = []
        for step in range(17):
            path.append([trajectory[1][step],trajectory[2][step]])
        traj = array(path).reshape(-1,2)

        print("**************** Intention probability calculation *****************")

        goal_probabilities = []

        for goal in range(len(patient_motion.ends[initial])):
            trajectories = patient_traj.scenario_set['trajs'][initial][goal]
            path = []
            dx = []
            for step in range(len(trajectories[0][0])-1):
                for t in trajectories:
                    path.append([t[1][step],t[2][step]])
                    dx.append([t[1][step+1]-t[1][step],t[2][step+1]-t[2][step]])

            data = [array(path).reshape(-1,2),array(dx).reshape(-1,2)]
            model = MGPR(data)

            i=0
            for m in model.models:
                gpflow.utilities.multiple_assign(m, patient_motion.GPs[initial][goal][i])
                i+=1
            goal_probabilities.append(model.predict_intention_probability(traj, patient_motion.probs[initial][goal]))

        for i in range(len(goal_probabilities[0])):
            sum_p = sum([goal_probabilities[j][i] for j in range(len(goal_probabilities))])
            for j in range(len(goal_probabilities)):
                goal_probabilities[j][i] = goal_probabilities[j][i]/sum_p
        print(goal_probabilities)
        plot_intent_prediction(background_filename, traj, goal_probabilities, patient_motion.ends[initial], env, trial)


    #
    # # ************************************ Overall evaluation **************************************************
    #
    # print("Final Scores Calculation...")
    # num = np.ones([env.numOfRows,env.numOfCols]) # Initializing number of points in each grid cell as one
    # for traj in TrajectoryPoints:
    #     for point in traj:
    #         [i,j] = fallRisk.meter2grid(point[0][0]) # Finding the grid cell for each point in trajectories
    #         fallRisk.scores[i,j] += point[1] # Add the score of that point to the associated grid cell
    #         num[i,j] += 1 # Add 1 to number of points inside that grid cell
    # for i in range(env.numOfRows):
    #     for j in range(env.numOfCols):
    #         fallRisk.scores[i,j] = fallRisk.scores[i,j]/num[i,j] # Take the avarage score for each grid cell
    # print("Final evaluation plot...")
    # fallRisk.plotDistribution(fallRisk.scores, png_filenames[5], pdf_filenames[5], 'hamming')
    #



    # ************************************ Risk-aware planning **************************************************

    #
    # num_trial = 1
    # trajectories = []
    # intention_set = {'start': ['Bed'], 'end': ['Toilet']}
    # dt = 0.5
    # num_points = 12
    # for intention in range(len(intention_set['start'])):
    #     for trial in range(num_trial):
    #         # Generating a trajectory for each intention each trial
    #         print("Trajectory prediction for intetion {0}, trial {1}: ".format(intention+1, trial+1))
    #         v_max = random.gauss(v[0], v[1])
    #         w_max = random.gauss(w[0], w[1])
    #         found = 0
    #         while found == 0:
    #             # Sample points near the start and end locations
    #             patient_s = sample_point(env, intention_set['start'][intention], obstacles)
    #             patient_g = sample_point(env, intention_set['end'][intention], obstacles)
    #             scenario = {'start': patient_s, 'end': patient_g, 'v_max': v_max, 'w_max': w_max}
    #             print("senario: ", scenario)
    #             # Find a trajectory between sampled points
    #             rbf_x, rbf_y, T, cost, predicted_patient_traj, status = OptPath_patient2(scenario['start'], scenario['end'],  [scenario['v_max'], scenario['w_max']] , obstacles, num_points, assistive_device=False)
    #             # If the optimization was successful, find the type of activity for each point on the trajectory and add it to the resturning path
    #             if status == 2 :
    #                 found = 1
    #                 time = [step * dt for step in range(int(T/dt))]
    #                 time_input = array(time).reshape(len(time), 1)
    #                 tau_x = rbf_x.find(time_input)
    #                 tau_y = rbf_y.find(time_input)
    #                 tau_before = [[tau_x[0], tau_y[0], 0, 0, 0]]
    #                 for i in range(1,int(T/dt)):
    #                     tau_before.append([tau_x[i], tau_y[i], math.atan2(tau_x[i]-tau_x[i-1],tau_y[i]-tau_y[i-1]), (tau_x[i]-tau_x[i-1])/dt, (tau_y[i]-tau_y[i-1])/dt])
    #                 for i in range(len(tau_before)):
    #                     if is_near_sitting_object(Point(tau_before[i]), env, intention_set['start'][intention]) :
    #                         tau_before[i].append('sit_to_stand')
    #                     elif is_near_sitting_object(Point(tau_before[i]), env, intention_set['end'][intention]):
    #                         tau_before[i].append('stand_to_sit')
    #                     else:
    #                         tau_before[i].append('walking')
    #         trajectories.append([tau_before, patient_s, patient_g, T])
    #
    # object_s = [3, 3, 0.001]
    # # object_f =
    # v_max_robot = [0.5, 0.5]
    # # walls =
    # # object parameters : Rho(4), Omega(3), width(1), length(1)   #Rho[0,1,2,3]=[mass, inertia, x_c, y_c]
    # object_params = [1.71238550e+01, 1.42854052e+02, -9.47588940e-01, -9.75868278e-02, 10.36983266, 92.26898084, 1.19346089, 0.54, 0.35]
    # object_r = np.sqrt(object_params[7]**2 +object_params[8]**2)/2
    # phi = [np.arctan2(-object_params[8] + object_params[3], - object_params[7] + object_params[2]) + np.pi,
	# 		 np.arctan2(-object_params[8] + object_params[3], object_params[2]), np.arctan2(-object_params[3], object_params[2]) + np.pi,
	# 		 np.arctan2(object_params[3], object_params[7] - object_params[2]) + np.pi]
    # r_max = [0,0,0,0]
    # for i in range(4):
    #     r_max[i] = np.sqrt((np.sign(i% 3)*object_params[7] - object_params[2])**2 + (np.sign(i/2)*object_params[8] - object_params[3])**2)
    #
    # object_leg = find_corners(object_s[0], object_s[1], object_s[2], object_params[7], object_params[8])
    # n = 20
    # force_max = 5
    #
    # for traj in trajectories:
    #     for intervention_ind in range(len(traj[0])):
    #         intervention_time = intervention_ind * dt
    #         intervention_point = traj[0][intervention_ind]
    #
    #         # Patient cost
    #             # fall score cost before intervetion
    #         print(traj[0])
    #         tau_before_score = fallRisk.getDistibutionForTrajectory(traj[0][0:intervention_ind+1], False, False)
    #         risk = [tau_before_score[i][1] for i in range(intervention_ind+1)]
    #
    #             # fall score cost after intervetion
    #         m = num_points - intervention_ind - 1
    #         found = 0
    #         while found == 0:
    #             rbf_x_new, rbf_y_new, T_new, cost, new_predicted_patient_traj, status = OptPath_patient2(intervention_point, traj[2], [random.gauss(v[0], v[1]), random.gauss(w[0], w[1])], obstacles, m, assistive_device=True)
    #             if status == 2:
    #                 found = 1
    #                 time_new = [step * dt for step in range(int(T_new/dt))]
    #                 print("time_new")
    #                 print(time_new)
    #                 time_input_new = array(time_new).reshape(len(time_new), 1)
    #                 tau_x_new = rbf_x_new.find(time_input_new)
    #                 tau_y_new = rbf_y_new.find(time_input_new)
    #                 tau_after = []
    #                 for i in range(1,int(T_new/dt)):
    #                     tau_after.append([tau_x_new[i], tau_y_new[i], math.atan2(tau_x_new[i]-tau_x_new[i-1],tau_y_new[i]-tau_y_new[i-1]), (tau_x_new[i]-tau_x_new[i-1])/dt, (tau_y_new[i]-tau_y_new[i-1])/dt])
    #                 for i in range(len(tau_after)):
    #                     if is_near_sitting_object(Point(tau_after[i]), env, traj[1]) :
    #                         tau_after[i].append('sit_to_stand')
    #                     elif is_near_sitting_object(Point(tau_after[i]), env, traj[2]):
    #                         tau_after[i].append('stand_to_sit')
    #                     else:
    #                         tau_after[i].append('walking')
    #                 print("tau_after")
    #                 print(tau_after)
    #         tau_after_score = fallRisk.getDistibutionForTrajectory(tau_after, False, True)
    #
    #         for step in range(len(tau_after)):
    #             risk.append(tau_after_score[step][1])
    #         print("risk")
    #         print(risk)
    #         combined_traj = traj[0][0:intervention_ind+1] + tau_after
    #         print("combined_traj")
    #         print(combined_traj)
    #         patient_cost = sum(risk)
    #         plotTrajDist(risk, combined_traj, background_filename)
    #
    #         # Robot cost
    #         cost_manipulation = []
    #         for leg in range(4):
    #             phi_linear = nominal_traj(object_s, intervention_point, v_max_robot, obstacles, n, dt)
    #             psi_linear = [phi_linear[k] + phi[leg] for k in range(len(phi_linear))]
    #         	# find the cost and required force for manipulation for the leg
    #             cost_manipulation.append(OptTraj(object_s, [intervention_point[0], intervention_point[1], 0], v_max_robot, obstacles, object_params[0:4], object_params[4:7], phi_linear, psi_linear, force_max, r_max[leg], n, dt, object_leg[leg]))
    #         robot_cost = min(cost_manipulation)
    #
    #         # Total cost
    #         total_cost = cost_robot + cost_patient
    #         costs.append([patient_cost, robot_cost, total_cost])
    #
    #     print(costs)
