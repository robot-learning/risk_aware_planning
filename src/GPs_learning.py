#!/usr/bin/env python


from Fall_risk_assesment import Environment_Image, FallRiskAssesment
import numpy as np
from scipy import *
from Gaussian_processes import MGPR
import random
from shapely.geometry import Point
import pickle
from visualization import plot_paths, plot_pred

class motion_set():
    def __init__(self, senario_set):
        self.starts = senario_set['start']
        self.ends = senario_set['end']
        self.probs = [[0.8, 0.2],
                 [0.7, 0.1, 0.2],
                 [0.8, 0.2],
                 [0.05, 0.6, 0.1, 0.2, 0.05],
                 [0.3, 0.2, 0.5],
                 [0.2, 0.2, 0.6],]
        self.GPs = [[], [], [], [], [], []]


if __name__ == '__main__':

    design_name = "Room-2-Inboard-Footwall"
    path = "/home/roya/catkin_ws/src/risk_aware_planning"
    background_filename = "{0}/Room_Designs/{1}_objects_rotated.png".format(path, design_name)

    # pickle_env = open("data/{0}/env.pickle".format(design_name),"rb")
    # env = pickle.load(pickle_env)
    # pickle_env.close()
    # pickle_baseline_evaluation = open("data/{0}/baseline_evaluation.pickle".format(design_name),"rb")
    # fall_risk = pickle.load(pickle_baseline_evaluation)
    # pickle_baseline_evaluation.close()
    pickle_trajectories = open("data/{0}/trajectories.pickle".format(design_name),"rb")
    patient_motion = pickle.load(pickle_trajectories)
    pickle_trajectories.close()

    print("**************** Learn GP Models *****************")
    # motion = motion_set(patient_motion.scenario_set)
    pickle_motion = open("data/{0}/motion_GPs.pickle".format(design_name),"rb")
    motion = pickle.load(pickle_motion)
    pickle_motion.close()

    # for initial in range(len(patient_motion.scenario_set['start'])):
    for initial in range(4,5):
        motion.GPs[initial] = []
        for goal in range(len(patient_motion.scenario_set['end'][initial])):
            trajectories = patient_motion.scenario_set['trajs'][initial][goal]
            path = []
            dx = []
            print("initial:", initial, "goal:", goal)
            for step in range(len(trajectories[0][0])-1):
                for traj in trajectories:
                    path.append([traj[1][step],traj[2][step]])
                    dx.append([traj[1][step+1]-traj[1][step],traj[2][step+1]-traj[2][step]])

            data = [array(path).reshape(-1,2),array(dx).reshape(-1,2)]

            initials_x = [data[0][i][0] for i in range(20)]
            initials_y = [data[0][i][1] for i in range(20)]
            m = array([mean(initials_x),mean(initials_y)])
            s =  array([[std(initials_x), 0],
                  [0, std(initials_y)]]) #0.05 * np.eye(2)
            n = 20
            model = MGPR(data)
            model.optimize(restarts=5)
            #
            # print("**************** Distribution Prediction *****************")
            # prediction = model.predict(m, s, n)
            # plot_pred(background_filename, prediction, trajectories)
            #
            # print("***************** Trajectory Prediction ******************")
            # trajs = []
            # K = 5
            # color = np.random.rand(3,)
            # for k in range(K):
            #     start = np.array([np.random.normal(m[0], s[0][0]), np.random.normal(m[1], s[1][1])])
            #     trajs.append(model.predict_path(start, n))
            # plot_paths(background_filename, trajs, color)

            # print("****************** Motion evaluation *********************")
            # TrajectoryPoints = []
            # counter = 0
            # intention_set={'start': ['Main Door'], 'end': ['Toilet']}
            # for traj in trajs:
            #         tau = []
            #         for i in range(len(traj[0])):
            #             tau.append([traj[0][i][0], traj[0][i][1], math.atan2(traj[1][i][1],traj[1][i][0]), traj[1][i][0], traj[1][i][1]])
            #             if is_near_sitting_object(Point(tau[i]), env, intention_set['start'][0]) :
            #                 tau[i].append('sit_to_stand')
            #             elif is_near_sitting_object(Point(tau[i]), env, intention_set['end'][0]):
            #                 tau[i].append('stand_to_sit')
            #             else:
            #                 tau[i].append('walking')
            #         # Evaluating the predicted trajectory
            #         TrajectoryPoints.append(fall_risk.getDistibutionForTrajectory(tau, False, False, counter, background_filename))
            #         counter += 1
            #
            # dt = 0.5
            # time_pred = np.linspace(0, dt*n, n)
            # for traj in TrajectoryPoints:
            #     fall_pred = [traj[i][1] for i in range(n)]
            #     plt.plot(time_pred, fall_pred)
            # plt.xlabel('time (s)')
            # plt.ylabel('Fall Risk Level')
            # plt.show()

            params_set = model.save_model()
            print(params_set)
            motion.GPs[initial].append(params_set)

    pickle_motion = open("data/{0}/motion_GPs.pickle".format(design_name),"wb")
    pickle.dump(motion, pickle_motion)
    pickle_motion.close()
